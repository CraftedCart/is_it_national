local get_buildings_with_class_stmt = DB.CreateQuery("SELECT Description FROM Buildings WHERE BuildingClass = ?")
local prefix_text_stmt = nil -- Set in init()
local placeholders = {} -- A table containing tables of placeholders to execute prefix_text_stmt with

local function is_national_wonder(building_class)
  return building_class.MaxPlayerInstances == 1
end

local function is_international_wonder(building_class)
  return building_class.MaxGlobalInstances == 1
end

local function is_team_wonder(building_class)
  return building_class.MaxTeamInstances == 1
end

local function prefix_building_text(class_type, prefix)
  -- Prefix the names of all buildings associated with a building class
  for building in get_buildings_with_class_stmt(class_type) do
    print("Prefix text queued: " .. building.Description)

    table.insert(placeholders, {prefix, building.Description})
  end
end

local function reload_texts()
  Locale.SetCurrentLanguage(Locale.GetCurrentLanguage().Type)
end

local function init()
  print("Initializing is_it_national")

  local start_time = os.time()

  local locale_code = Locale.GetCurrentLanguage().Type
  prefix_text_stmt =
    DB.CreateQuery("UPDATE Language_" ..
    locale_code..
    " SET Text = ? || (SELECT Text FROM Language_" ..
    locale_code ..
    " WHERE Tag = ?) WHERE Tag = ?")

  -- We batch up all db transactions for speed
  for result in DB.Query("BEGIN TRANSACTION") do end

  -- Search for wonders by checking each building class
  for cls in GameInfo.BuildingClasses() do
    local type = cls.Type

    if is_national_wonder(cls) then
      print("Found national wonder: " .. type)
      prefix_building_text(type, "[ICON_CITY_STATE] ")
    elseif is_international_wonder(cls) then
      print("Found international wonder: " .. type)
      prefix_building_text(type, "[ICON_GOLDEN_AGE] ")
    elseif is_team_wonder(cls) then
      print("Found team wonder: " .. type)
      prefix_building_text(type, "[ICON_TEAM_1] ")
    end
  end

  print("Prefixing all texts")
  for k, v in pairs(placeholders) do
    for result in prefix_text_stmt(v[1], v[2], v[2]) do end
  end

  -- Write all changes to the db
  for result in DB.Query("COMMIT") do end

  print("Reloading language texts")
  reload_texts()

  -- Free up some memory
  get_buildings_with_class_stmt = nil
  prefix_text_stmt = nil
  placeholders = nil

  local end_time = os.time()
  local elapsed_time = os.difftime(end_time, start_time)
  print("Finished initializing is_it_national in " .. elapsed_time .. "s")
end

Events.SequenceGameInitComplete.Add(init)
